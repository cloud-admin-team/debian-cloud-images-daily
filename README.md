# Daily handling for Debian Cloud images

## Environment variables

Environment variables needs to be configured in the GitLab web interface.

 * `$UPLOAD_ENABLED`: Set to `1` to upload to Debian storage.
 * `$UPLOAD_AZURE_ENABLED`: Set to `1` to upload Azure images.
 * `$UPLOAD_AZURE_NOTIFY_EMAIL`: Run Azure publish process and send report to given e-mail address.
 * `$UPLOAD_EC2_ENABLED`: Set to `1` to upload EC2 images.
 * `$UPLOAD_SSH_KEY`
 * `$UPLOAD_SSH_REMOTE`
